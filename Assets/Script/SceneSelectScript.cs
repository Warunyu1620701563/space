using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSelectScript : MonoBehaviour
{
   public void selectScene()
    {
        switch (this.gameObject.name)
        {
           case "Button1":
                SceneManager.LoadScene("Scene1");
                break;
            case "Button2":
                SceneManager.LoadScene("Scene2");
                break;
            case "Button":
                SceneManager.LoadScene("MainMenu");
                break;
        }
    }
}
