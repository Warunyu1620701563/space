using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShooting : MonoBehaviour
{
    public Vector3 bulletOffset = new Vector3(0, 1.3f, 0);
    public GameObject bullerPrefab;

    public float fireDelay = 0.25f;
    float cooldownTimer = 0;
    void Update()
    {
        cooldownTimer -= Time.deltaTime;
        if (cooldownTimer <= 0)
        {
            Debug.Log("Pew!");
            cooldownTimer = fireDelay;
            Vector3 offset = transform.rotation * bulletOffset;
            GameObject bulletGo = (GameObject)Instantiate(bullerPrefab, transform.position + offset, transform.rotation);
            bulletGo.layer = gameObject.layer;
        }
    } 
}
